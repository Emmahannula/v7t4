package com.example.v7t4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Scanner;


public class MainActivity extends AppCompatActivity {
    TextView text1;
    EditText text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = (TextView) findViewById(R.id.textView);
        EditText editText = (EditText)findViewById(R.id.editText);
        editText.setText("Anna syöte");

        editText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                text2 = findViewById(R.id.editText);
                text1.setText(text2.getText().toString());
            }
        });

    }
}
